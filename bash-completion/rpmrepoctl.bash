#!/usr/bin/bash
# Bash Completion scriptlet for rpmrepoctl
#
# Author: Daniel J. R. May <daniel.may@danieljrmay.com>
# Version: 0.10

function _rpmrepoctl()
{
    # shellcheck disable=SC2034
    # words and cword are defined by _init_completion
    local cur prev words cword split
    _init_completion -s || return

    local options='--arch --assumeyes --basearch -d --debug --deltas -h --help --keep --releasever --repo --repos-config-dir --skip-update -v --verbose -y'
    local commands='add clean create demote destroy help list promote pull push remove repoinfo repolist rpminfo update'

    if [[ $cur = -* ]]
    then
	mapfile -t COMPREPLY < <(compgen -W "$options" -- "$cur")
	return
    fi

    case $prev in
	rpmrepoctl|-d|--debug|-v|--verbose|-y|--assumeyes|--skip-update|--deltas)
	    mapfile -t COMPREPLY < <(compgen -W "$options $commands" -- "$cur")
	    return
	    ;;
	--arch)
	    local arches
	    arches=$(python3 -c 'from rpmrepoctl.variables import Variables; print(" ".join(Variables.VALID_ARCH_VALUES))')
	    mapfile -t COMPREPLY < <(compgen -W "$arches" -- "$cur")
	    return
	    ;;
	--basearch)
	    local basearches
	    basearches=$(python3 -c 'from rpmrepoctl.variables import Variables; print(" ".join(Variables.VALID_BASEARCH_VALUES))')
	    mapfile -t COMPREPLY < <(compgen -W "$basearches" -- "$cur")
	    return
	    ;;
	--keep)
	    local digits
	    digits=$(seq 0 9)
	    mapfile -t COMPREPLY < <(compgen -W "$digits" -- "$cur")
	    return
	    ;;
	--releasever)
	    local releasevers
	    releasevers=$(python3 -c 'from rpmrepoctl.variables import Variables; r = range(Variables.VALID_RELEASEVER_MIN, Variables.VALID_RELEASEVER_MAX); print(" ".join([str(i) for i in r]))')
	    mapfile -t COMPREPLY < <(compgen -W "$releasevers" -- "$cur")
	    return
	    ;;
	--repo)
	    local repos
	    repos=$(rpmrepoctl repolist)
	    mapfile -t COMPREPLY < <(compgen -W "$repos" -- "$cur")
	    return
	    ;;
	--repos-config-dir)
	    _filedir -d
	    return
	    ;;
	-h|--help)
	    return
	    ;;
	add|demote|promote|remove|rpminfo|*.rpm)
	    _filedir "*.rpm"
	    return
	    ;;
	clean|create|destroy|help|list|pull|push|repoinfo|repolist|update)
	    return
	    ;;
    esac
}
complete -F _rpmrepoctl rpmrepoctl
