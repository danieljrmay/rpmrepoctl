# rpmrepoctl

A utility to aid in the administration any RPM repositories which you
host.

## Development

### Python Virtual Environments

You can create and activate a Python virtual environment to develop in
with the following Bash commands.

```sh
python3 -m venv venv
source venv/bin/activate
pip install --upgrade pip
pip install rope jedi flake8 autopep8 yapf
```

This creates an environment with the following packages which are
useful for development.

* **rope**: a refactoring library for Python.
* **jedi**: autocompletion library for Python.
* **flake8**: style guide enforcement for Python.
* **autopep8**: source code formatting for Python.
* **yapf**: another formatter for Python.

**rpmrepoctl** does not require anything which is not already in the
Python standard libraries.

#### Using the virtual environment in Bash

Once created you can activate the virtual environment from the Bash
command line with:

```sh
source venv/bin/activate
```

You can deactivate the virtual environment with:

```sh
deactivate
```

#### Using the virtual environment in Emacs

You can activate the virtual environment within Emacs with `M-x
pyvenv-activate` and then enter the path to the `venv` directory.
