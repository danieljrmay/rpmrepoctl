%global srcname rpmrepoctl

Name:           python-%{srcname}
Version:        0.10
Release:        1%{?dist}
Summary:        Tool for curating the RPM repositories you host

License:        GPLv3
URL:            https://gitlab.com/danieljrmay/%{srcname}
Source0:        %{url}/-/archive/%{version}/%{srcname}-%{version}.tar.bz2

BuildArch:      noarch


%description
Tool for curating the RPM repositories you host.


%package -n python3-%{srcname}
Summary:       Python 3 build of the rpmrepoctl module
BuildRequires: python3-devel python3-distro python3-dnf python3-gnupg python3-rpm
Requires:      createrepo_c dnf-utils python3-distro python3-dnf python3-gnupg rsync
%{?python_provide:%python_provide python3-%{srcname}}

%description -n python3-%{srcname}
The Python 3 module which provides the core functionality for the
rpmrepoctl tool.


%package -n %{srcname}
Summary:  %{summary}
Requires: bash-completion python3-%{srcname} = %{version}-%{release}

%description -n %{srcname}
Tool for curating the RPM repositories you host.


%prep
%autosetup -n %{srcname}-%{version}


%build
%py3_build
%make_build


%install
%py3_install
%make_install


%check
%{__python3} setup.py test


%files -n python3-%{srcname}
%doc README.md
%license LICENSE
%{python3_sitelib}/*


%files -n %{srcname}
%doc README.md
%license LICENSE
%{_bindir}/%{srcname}
%{_mandir}/man1/%{srcname}.1.gz
%{_datadir}/bash-completion/completions/%{srcname}


%changelog
* Thu Jul 18 2019 Daniel J. R. May <daniel.may@danieljrmay.com> - 0.10-1
- Add repoinfo command.

* Wed Jul 17 2019 Daniel J. R. May <daniel.may@danieljrmay.com> - 0.9-1
- Fix broken tests in Fedora 30.

* Wed Jul 17 2019 Daniel J. R. May <daniel.may@danieljrmay.com> - 0.8-1
- Fix RPM build requirements.
- Remove debug code from rpminfo output.

* Wed Jul 17 2019 Daniel J. R. May <daniel.may@danieljrmay.com> - 0.7-1
- Fix python3-gnupg requirements.

* Fri Jul 12 2019 Daniel J. R. May <daniel.may@danieljrmay.com> - 0.6-1
- Add rpminfo command.
- Fixed basic bash completion script.
- Fixed entry point.
- Fixed extra newlines in list command output.

* Wed Jul 10 2019 Daniel J. R. May <daniel.may@danieljrmay.com> - 0.5-1
- Fix requirements specification and install bash-completion script.

* Wed Jul 10 2019 Daniel J. R. May <daniel.may@danieljrmay.com> - 0.4-1
- Fix requirements in spec file.

* Wed Jul 10 2019 Daniel J. R. May <daniel.may@danieljrmay.com> - 0.3-1
- Fix console script configuration in setup.py file.

* Tue Jul  9 2019 Daniel J. R. May <daniel.may@danieljrmay.com> - 0.2-1
- Fixed tests to work with rawhide RPM build.

* Fri Jul  5 2019 Daniel J. R. May <daniel.may@danieljrmay.com> - 0.1-1
- Initial release.
