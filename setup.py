"""setuptools configuration for the rpmrepoctl package.

See: https://packaging.python.org/tutorials/packaging-projects
"""

import setuptools

with open("python-rpmrepoctl.spec", "r") as fh:
    for line in fh:
        if line.startswith("Version:"):
            VERSION = line[8:].strip()
            break

with open("README.md", "r") as fh:
    LONG_DESCRIPTION = fh.read()

setuptools.setup(
    name="rpmrepoctl",
    version=VERSION,
    author="Daniel J. R. May",
    author_email="daniel.may@danieljrmay.com",
    description="Curate RPM repositories which you host",
    long_description=LONG_DESCRIPTION,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/danieljrmay/rpmrepoctl",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: OS Independent",
        "Development Status :: 3 - Alpha",
    ],
    entry_points={
        "console_scripts": ["rpmrepoctl = rpmrepoctl.__main__:main"],
    },
    test_suite="rpmrepoctl.tests",
    include_package_data=False,
)
