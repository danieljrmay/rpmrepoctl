#!/usr/bin/env python3

"""Tests the rpmrepoctl.py module."""

import os
import unittest

from rpmrepoctl.rpmgpgkey import RpmGpgKey


class TestRpmGpgKey(unittest.TestCase):
    """ Test the RpmGpgKey class."""

    def setUp(self):
        """Construct objects used for testing."""
        self.directory = "rpmrepoctl/tests/rpm-gpg"
        self.fc30_keyfile_path = os.path.join(
            self.directory, "RPM-GPG-KEY-fedora-30-primary")

    def test_get_keys(self):
        """Test get_keyids(paths) static method."""
        print(RpmGpgKey.get_keys(self.fc30_keyfile_path))

    def test_get_key_file_paths(self):
        """Test get_key_file_paths(directory) static method."""
        paths = RpmGpgKey.get_key_file_paths(self.directory)
        self.assertTrue(self.fc30_keyfile_path in paths)

    def test_file_contains_key(self):
        """Test file_contains_key(path, keyid) static method."""
        self.assertFalse(
            RpmGpgKey.file_contains_key(
                os.path.join(self.directory, "RPM-GPG-KEY-fedora-30-primary"),
                "XXXXXXXX")
        )
        self.assertTrue(
            RpmGpgKey.file_contains_key(
                os.path.join(self.directory, "RPM-GPG-KEY-fedora-30-primary"),
                "CFC659B9")
        )

    def test_find_key_file_path(self):
        """Test find_key_file_path(keyid, directory) static method."""
        self.assertEqual(
            RpmGpgKey.find_key_file_path("CFC659B9", self.directory),
            os.path.join(self.directory, "RPM-GPG-KEY-fedora-30-primary")
        )


if __name__ == '__main__':
    unittest.main()
