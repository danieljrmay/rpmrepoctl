#!/usr/bin/env python3

"""Tests the rpmrepoctl.py module."""

import logging
import random
import unittest
from rpmrepoctl.rpmrepoctl import RpmRepoCtl
from rpmrepoctl.tests.utils import get_random_string
from rpmrepoctl.variables import Variables


class TestRpmRepoCtl(unittest.TestCase):
    """Test the RpmRepoCtl class."""

    def setUp(self):
        """Construct objects used for testing."""
        self.basearch = random.choice(Variables.VALID_BASEARCH_VALUES)
        self.releasever = random.randint(
            Variables.VALID_RELEASEVER_MIN,
            Variables.VALID_RELEASEVER_MAX)

        parser = RpmRepoCtl.get_argument_parser()
        args = parser.parse_args([
            '--repos-config-dir', 'rpmrepoctl/tests/good_repo_configs',
            '--basearch', self.basearch,
            '--releasever', str(self.releasever),
            'help'
            ])

        logging.basicConfig(level=logging.DEBUG,
                            filename="test.log",
                            filemode="w")

        self.rpmrepoctl = RpmRepoCtl(args)

    def test_repo_id_list(self):
        """Test the repo_id_list property."""
        expected_repo_id_list = ["good-repo",
                                 "good-repo-source",
                                 "good-repo-testing",
                                 "good-repo-testing-source"]

        for repo_id in expected_repo_id_list:
            self.assertIn(repo_id, self.rpmrepoctl.repo_id_list)

    def test_has_repo_id(self):
        """Test the has_repo_id(repo_id) method."""
        for repo_id in self.rpmrepoctl.repo_id_list:
            self.assertTrue(self.rpmrepoctl.has_repo_id(repo_id))

        for i in range(10):
            random_str = get_random_string(i)
            if random_str in self.rpmrepoctl.repo_id_list:
                continue
            else:
                self.assertFalse(self.rpmrepoctl.has_repo_id(random_str))


if __name__ == '__main__':
    unittest.main()
