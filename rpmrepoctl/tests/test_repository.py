#!/usr/bin/env python3

"""Tests the rpmrepoctl.py module."""

import configparser
import logging
import os
import shutil
import unittest

from rpmrepoctl.repository import Repository


class TestRespository(unittest.TestCase):
    """ Test the Repository class."""

    def setUp(self):
        """Construct objects used for testing."""
        logging.basicConfig(level=logging.DEBUG,
                            filename="test.log",
                            filemode="w")

        variables = {'arch': 'x86-64',
                     'basearch': 'x86-64',
                     'releasever': '29'}
        config = configparser.ConfigParser(
            variables,
            interpolation=configparser.ExtendedInterpolation())
        config.read("rpmrepoctl/tests/test_rpmrepoctl.d/test.rpmrepoctl")

        self.repo_a = Repository("test-a", config)

        if os.path.exists("/tmp/rpmrepoctl"):
            shutil.rmtree("/tmp/rpmrepoctl")

    def test_content(self):
        """Test the content property."""
        self.assertEqual(self.repo_a.content, "binary-x86-64")

    def test_demote_repo_ids(self):
        """Test the demote_repo_ids property."""
        self.assertEqual(self.repo_a.demote_repo_ids, ['test-a-testing'])

    def test_dist(self):
        """Test the dist property."""
        self.assertEqual(self.repo_a.dist, '.fc29')

    def test_distro(self):
        """Test the distro property."""
        self.assertEqual(self.repo_a.distro, 'cpe:/o:fedoraproject:fedora:29')

    def test_downsream_urls(self):
        """Test the downstream_urls property."""
        self.assertEqual(
            self.repo_a.downstream_urls,
            ['user@example.org:/srv/rpm/test-a/fedora/29/x86-64'])

    def test_enabled(self):
        """Test the enabled property."""
        self.assertEqual(self.repo_a.enabled, True)

    def test_exists(self):
        """Test the exists property."""
        self.assertEqual(self.repo_a.exists, False)

    def test_gpgcheck(self):
        """Test the gpgcheck property."""
        self.assertEqual(self.repo_a.gpgcheck, True)

    def test_gpgkey(self):
        """Test the gpgkey property."""
        self.assertEqual(
            self.repo_a.gpgkey,
            'file:///etc/pki/rpm-gpg/RPM-GPG-KEY-test')

    def test_keep(self):
        """Test the keep property."""
        self.assertEqual(self.repo_a.keep, 0)

    def test_name(self):
        """Test the name property."""
        self.assertEqual(self.repo_a.name, 'Test Repo A — Fedora 29 — x86-64')

    def test_path(self):
        """Test the path property."""
        self.assertEqual(
            self.repo_a.path,
            '/tmp/rpmrepoctl/test-a/fedora/29/x86-64')

    def test_promote_repo_ids(self):
        """Test the promote_repo_ids property."""
        self.assertEqual(self.repo_a.promote_repo_ids, [])

    def test_repo_id(self):
        """Test the repo_id property."""
        self.assertEqual(self.repo_a.repo_id, 'test-a')

    def test_repo_gpgcheck(self):
        """Test the repo_gpgcheck property."""
        self.assertEqual(self.repo_a.repo_gpgcheck, False)

    def test_tags(self):
        """Test the tags property."""
        self.assertEqual(self.repo_a.tags, "stable")

    def test_type(self):
        """Test the type property."""
        self.assertEqual(self.repo_a.type, "rpm")

    def test_upsream_urls(self):
        """Test the upstream_urls property."""
        self.assertEqual(
            self.repo_a.upstream_urls,
            [])

    def test_can_contain_binary_rpm(self):
        """Test the can_contain_binary_rpm(arch) method."""
        self.assertTrue(self.repo_a.can_contain_binary_rpm("x86-64"))

    def test_can_contain_source_rpm(self):
        """Test the can_contain_source_rpm(arch) method."""
        self.assertFalse(self.repo_a.can_contain_source_rpm())

    # Implement tests for: add clean contains create demote destroy
    # is_compatible list old_rpms_paths promote pull push remove
    # update _create_directory


if __name__ == '__main__':
    unittest.main()
