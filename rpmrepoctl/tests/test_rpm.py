#!/usr/bin/env python3

"""Tests the rpmrepoctl.py module."""

import unittest

from rpmrepoctl.rpm import Rpm
from rpmrepoctl.variables import Variables


class TestRpm(unittest.TestCase):
    """ Test the Rpm class."""

    def setUp(self):
        """Construct objects used for testing."""
        self.variables = Variables()

    def test_good_binary_x86_64_rpm(self):
        """Check the metadata of a signed, valid, binary RPM."""
        if self.variables.releasever == "29":
            filename = "basesystem-11-6.fc29.noarch.rpm"
            path = "rpmrepoctl/tests/" + filename
            rpmmd = Rpm(path)

            self.assertEqual(rpmmd.filename, filename)
            self.assertFalse(rpmmd.is_source)
            self.assertEqual(rpmmd.name, "basesystem")
            self.assertEqual(rpmmd.version, "11")
            self.assertEqual(rpmmd.release, "6.fc29")
            self.assertEqual(rpmmd.arch, "noarch")
            self.assertEqual(rpmmd.specver, "6")
            self.assertEqual(rpmmd.dist, ".fc29")
            self.assertEqual(rpmmd.distro_code, "fc")
            self.assertEqual(rpmmd.releasever, "29")
            self.assertTrue("a20aa56b429476b4" in rpmmd.signature)
        elif self.variables.releasever == "30":
            filename = "basesystem-11-7.fc30.noarch.rpm"
            path = "rpmrepoctl/tests/" + filename
            rpmmd = Rpm(path)

            self.assertEqual(rpmmd.filename, filename)
            self.assertFalse(rpmmd.is_source)
            self.assertEqual(rpmmd.name, "basesystem")
            self.assertEqual(rpmmd.version, "11")
            self.assertEqual(rpmmd.release, "7.fc30")
            self.assertEqual(rpmmd.arch, "noarch")
            self.assertEqual(rpmmd.specver, "7")
            self.assertEqual(rpmmd.dist, ".fc30")
            self.assertEqual(rpmmd.distro_code, "fc")
            self.assertEqual(rpmmd.releasever, "30")
            self.assertTrue("ef3c111fcfc659b9" in rpmmd.signature)
        else:
            print("\nNo test for releasever=" + self.variables.releasever)

    def test_good_source_rpm(self):
        """Check the metadata of a signed, valid, source RPM."""
        if self.variables.releasever == "29":
            filename = "basesystem-11-6.fc29.src.rpm"
            path = "rpmrepoctl/tests/" + filename
            rpmmd = Rpm(path)

            self.assertEqual(rpmmd.filename, filename)
            self.assertTrue(rpmmd.is_source)
            self.assertEqual(rpmmd.name, "basesystem")
            self.assertEqual(rpmmd.version, "11")
            self.assertEqual(rpmmd.release, "6.fc29")
            self.assertEqual(rpmmd.arch, "noarch")
            self.assertEqual(rpmmd.specver, "6")
            self.assertEqual(rpmmd.dist, ".fc29")
            self.assertEqual(rpmmd.distro_code, "fc")
            self.assertEqual(rpmmd.releasever, "29")
            self.assertTrue("a20aa56b429476b4" in rpmmd.signature)
        elif self.variables.releasever == "30":
            filename = "basesystem-11-7.fc30.src.rpm"
            path = "rpmrepoctl/tests/" + filename
            rpmmd = Rpm(path)

            self.assertEqual(rpmmd.filename, filename)
            self.assertTrue(rpmmd.is_source)
            self.assertEqual(rpmmd.name, "basesystem")
            self.assertEqual(rpmmd.version, "11")
            self.assertEqual(rpmmd.release, "7.fc30")
            self.assertEqual(rpmmd.arch, "noarch")
            self.assertEqual(rpmmd.specver, "7")
            self.assertEqual(rpmmd.dist, ".fc30")
            self.assertEqual(rpmmd.distro_code, "fc")
            self.assertEqual(rpmmd.releasever, "30")
            self.assertTrue("ef3c111fcfc659b9" in rpmmd.signature)
        else:
            print("\nNo test for releasever=" + self.variables.releasever)


if __name__ == '__main__':
    unittest.main()
