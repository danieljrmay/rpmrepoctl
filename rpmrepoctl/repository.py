"""Repository configuration and actions."""

import logging
import glob
import os
import shutil
import subprocess


class Repository():
    """A representation of a repository."""

    def __init__(self, repo_id, config):
        """Construct a repository."""
        logging.debug("Constructing repository %s.", repo_id)

        self._repo_id = repo_id
        self._config = config

        if self.gpgkey is None \
           and (self.gpgcheck or self.repo_gpgcheck):
            raise ValueError(
                "gpgcheck or repo_gpgcheck is enabled for {} "
                "but no gpgkey is defined.".format(repo_id))

    @property
    def content(self):
        """Return repository content tags e.g. "source", etc."""
        return self._config.get(self.repo_id, "content", fallback=None)

    @property
    def demote_repo_ids(self):
        """Return a list of repository Ids.

        When demoting RPMs from this repository they are moved into
        the repositories represented by the Ids in the returned list.
        """
        return self._config.get(self.repo_id, "demote_to", fallback="").split()

    @property
    def dist(self):
        """Return the repository dist e.g. ".fc29", ".el7", etc."""
        return self._config.get(self.repo_id, "dist", fallback=None)

    @property
    def distro(self):
        """Return the distro tag e.g. "cpe:/o:fedoraproject:fedora:29"."""
        return self._config.get(self.repo_id, "distro", fallback=None)

    @property
    def downstream_urls(self):
        """Return a list of downstream_urls."""
        return self._config.get(
            self.repo_id, "downstream_urls", fallback="").split()

    @property
    def enabled(self):
        """Return True if repository is enabled."""
        return self._config.getboolean(self.repo_id, "enabled")

    @property
    def exists(self):
        """Return True if repository exists on disk."""
        if os.path.exists(self.path + "/repodata/repomd.xml"):
            return True

        return False

    @property
    def gpgcheck(self):
        """Return True if repository RPMs should be signed and checked."""
        return self._config.getboolean(self.repo_id, "gpgcheck", fallback=True)

    @property
    def gpgkey(self):
        """Return absolute path to GPG public key."""
        return self._config.get(self.repo_id, "gpgkey", fallback=None)

    @property
    def keep(self):
        """Return number of old RPMs to keep when cleaning."""
        return self._config.getint(self.repo_id, "keep", fallback=0)

    @property
    def name(self):
        """Return the full repository name."""
        return self._config.get(self.repo_id, "name")

    @property
    def path(self):
        """Return the repository absolute path."""
        return self._config.get(self.repo_id, "path")

    @property
    def promote_repo_ids(self):
        """Return a list of repository Ids.

        When promoting RPMs from this repository they are moved into
        the repositories represented by the Ids in the returned list.
        """
        return self._config.get(
            self.repo_id, "promote_to", fallback="").split()

    @property
    def repo_id(self):
        """Return the repository Id."""
        return self._repo_id

    @property
    def repo_gpgcheck(self):
        """Return True if repository metadata file should be signed."""
        return self._config.getboolean(
            self.repo_id, "repo_gpgcheck", fallback=False)

    @property
    def tags(self):
        """Return tags which describe the repository e.g. "stable"."""
        return self._config.get(self.repo_id, "tags", fallback=None)

    @property
    def type(self):
        """Return the type of repository, typically "rpm"."""
        return self._config.get(self.repo_id, "type", fallback="rpm-md")

    @property
    def upstream_urls(self):
        """Return a list of upstream_urls."""
        return self._config.get(
            self.repo_id, "upstream_urls", fallback="").split()

    def can_contain_binary_rpm(self, arch):
        """Return True if repository can contain binary RPMs of specified arch.

        If no content tag is specified or it contains any of "source",
        "src", "srpm" or "any" then we return True. All comparisons
        are case-insensitive.
        """
        if self.content is None:
            return True

        lower_case_content = self.content.lower()
        if (
                arch == "noarch" and
                (
                    "binary" in lower_case_content or
                    "bin" in lower_case_content or
                    "rpm" in lower_case_content or
                    "any" in lower_case_content
                )
        ):
            return True

        if (
                arch in lower_case_content or
                "any" in lower_case_content
        ):
            return True

        return False

    def can_contain_source_rpm(self):
        """Return True if repository can contain source RPMs.

        If no content tag is specified or it contains any of "source",
        "src", "srpm" or "any" then we return True. All comparisons
        are case-insensitive.
        """
        if self.content is None:
            return True

        lower_case_content = self.content.lower()
        if (
                "source" in lower_case_content or
                "src" in lower_case_content or
                "srpm" in lower_case_content or
                "any" in lower_case_content
        ):
            return True

        return False

    def add(self, rpms, skip_update=False):
        """Add specified RPMs to this repository."""
        if not self.exists:
            logging.info(
                "Need to create %s before adding RPMs to it.", self.repo_id)
            self.create()

        update_required = False

        for rpm in rpms:
            if self.contains([rpm]):
                logging.info(
                    "Skipping add of %s to %s because it "
                    "is already in the repository.",
                    rpm.filename, self.repo_id)
            elif self.is_compatible(rpm):
                shutil.copy(rpm.path, self.path)
                update_required = True
            else:
                logging.warning(
                    "Skipping add of %s to %s because it "
                    "is not compatible with the repository.",
                    rpm.filename, self.repo_id)

        if update_required:
            self.update(skip_update)
        else:
            logging.debug(
                "Skipping update of %s because it has not been changed.",
                self.repo_id)

    def clean(self, skip_update=False, old_rpms_to_keep=None):
        """Clean old RPMs from this repository.

        If the repository does not exist on disk then nothing is done.

        The repository is automatically updated on disk, unless
        skip_update=True.

        If specifed the old_rpms_to_keep argument overrides the value
        provided by the repositories configuration file.

        """
        if not self.exists:
            logging.info(
                "Skipping cleaning of %s because it has "
                "not been created.", self.repo_id)
            return

        old_rpms_paths = self.old_rpms_paths(old_rpms_to_keep)

        for rpm_path in old_rpms_paths:
            logging.debug("Cleaning %s from %s.", rpm_path, self.repo_id)
            os.remove(rpm_path)

        logging.info(
            "Cleaned %s old RPMs "
            "from %s.", len(old_rpms_paths), self.repo_id)

        if not old_rpms_paths:
            logging.info("Skipping unrequired update of %s.", self.repo_id)
            return

        self.update(skip_update)

    def contains(self, rpms):
        """Return True if this repository contains all the specifed RPMs."""
        repo_dir = os.path.abspath(self.path)
        for rpm in rpms:
            if os.path.abspath(os.path.dirname(rpm.path)) != repo_dir:
                return False

        return True

    def create(self):
        """Create this repository on disk.

        If the repository already exists on disk nothing is done.
        """
        if self.exists:
            logging.info(
                "Skipping creation of %s, because it has "
                "already been created.", self.repo_id)
            return

        self._create_directory()
        cmd = ["createrepo_c", "--quiet"]

        if self.distro:
            cmd.append("--distro")
            cmd.append(self.distro)

        if self.content:
            cmd.append("--content")
            cmd.append(self.content)

        if self.tags:
            cmd.append("--repo")
            cmd.append(self.tags)

        cmd.append(self.path)
        logging.info("Creating repository %s.", self.repo_id)
        subprocess.run(cmd, check=True)

    def demote(self, rpms, skip_update=False):
        """Demote a list of RPMs from this repository."""
        if not self.contains(rpms):
            raise ValueError(
                "Not all of the specified RPMs "
                "exist in {}".format(self.repo_id))

        for demote_repo_id in self.demote_repo_ids:
            demote_repo = Repository(demote_repo_id, self._config)
            demote_repo.add(rpms, skip_update)

        self.remove(rpms, skip_update)

    def destroy(self):
        """Destroy this repository on disk."""
        if self.exists:
            shutil.rmtree(self.path)
        else:
            logging.info(
                "Skipping destruction of %s, because it does "
                "not exist.", self.repo_id)

    def is_compatible(self, rpm):
        """Return True if specifed RPM is compatible with this repository."""
        if rpm.dist != self.dist:
            return False

        if rpm.is_source and self.can_contain_source_rpm():
            return True

        if not rpm.is_source and self.can_contain_binary_rpm(rpm.arch):
            return True

        return False

    def list(self):
        """Return this repositories RPMs as a list of filenames."""
        if not self.exists:
            logging.warning(
                "Repository %s has no RPMs because it does "
                "not exist.", self.repo_id)
            return []

        return glob.glob(self.path + "/*.rpm")

    def old_rpms_paths(self, old_rpms_to_keep=None):
        """Return a list of old RPMs absolute paths."""
        if not self.exists:
            logging.info(
                "Repository %s has no old RPMs because it has "
                "not been created.", self.repo_id)
            return []

        if not glob.glob(self.path + "/*.rpm"):
            logging.info(
                "Repository %s contains no RPMs.", self.repo_id)
            return []

        if old_rpms_to_keep is None:
            keep = self.keep
        else:
            keep = int(old_rpms_to_keep)

        cmd = ["repomanage", "--quiet", "--old",
               "--keep", str(keep),
               self.path]
        completed_process = subprocess.run(
            cmd, check=True, capture_output=True)

        return completed_process.stdout.decode('utf-8').splitlines()

    def promote(self, rpms, skip_update=False):
        """Promote a list of RPMs from this repository."""
        if not self.contains(rpms):
            raise ValueError(
                "Not all of the specified RPMs "
                "exist in {}".format(self.repo_id))

        for promote_repo_id in self.promote_repo_ids:
            promote_repo = Repository(promote_repo_id, self._config)
            promote_repo.add(rpms, skip_update)

        self.remove(rpms, skip_update)

    def pull(self, skip_update=False):
        """Pull RPMs from all upstream repositories to this repository."""
        for upstream in self.upstream_urls:
            upstream_src = upstream + "/"
            cmd = ["rsync", "--include='*.rpm'", upstream_src, self.path]
            logging.info(
                "Pulling RPMs from %s into %s.", upstream, self.repo_id)
            logging.debug("Running command: %s", cmd)
            subprocess.run(cmd, check=True)

        self.update(skip_update)

    def push(self, skip_update=False):
        """Push RPMs from this repository to all downstream repositories."""
        for downstream in self.downstream_urls:
            cmd = ["rsync"]
            rpm_paths = glob.glob(self.path + "/*.rpm")
            cmd.extend(rpm_paths)
            cmd.append(downstream + "/")
            logging.info(
                "Pushing RPMs from %s to %s.", self.repo_id, downstream)
            logging.debug("Running command: %s", cmd)
            subprocess.run(cmd, check=True)

            if not skip_update:
                logging.warning("Remote update has not been implemented.")

    def remove(self, rpms, skip_update=False):
        """Remove the list of RPMs from this repository."""
        if not self.contains(rpms):
            raise ValueError(
                "Some of the specified RPMs are not "
                "in {}.".format(self.repo_id))

        for rpm in rpms:
            logging.info("Removing %s from %s.", rpm.path, self.repo_id)
            os.remove(rpm.path)

        self.update(skip_update)

    def update(self, skip_update=False):
        """Update this repository on disk.

        If the repository does not exist it is created.
        """
        if skip_update:
            logging.warning(
                "Skipping update of %s, repository "
                "metadata is now out-of-date with RPMs in "
                "repository directory.", self.repo_id)
            return

        if not self.exists:
            self.create()
            return

        cmd = ["createrepo_c", "--update", "--quiet"]

        if self.distro:
            cmd.append("--distro")
            cmd.append(self.distro)

        if self.content:
            cmd.append("--content")
            cmd.append(self.content)

        if self.tags:
            cmd.append("--repo")
            cmd.append(self.tags)

        cmd.append(self.path)
        logging.info("Updating repository %s.", self.repo_id)
        subprocess.run(cmd, check=True)

    def _create_directory(self):
        """Create this repositories directory if it does not already exist."""
        if not os.path.isdir(self.path):
            logging.debug(
                "Making directory %s for "
                "repository %s.", self.path, self.repo_id)
            os.makedirs(self.path)

        if not os.access(self.path, os.W_OK):
            raise PermissionError(
                "Repository {} does not have write access to its "
                "directory {}".format(self.repo_id, self.path))

    def __str__(self):
        """Return an information about the current repository.

        This magic method controls the output of str(rpm) and
        print(rpm).
        """
        return ("Id:\t\t" + self.repo_id +
                "\nName:\t\t" + self.name +
                "\nPath:\t\t" + self.path +
                "\nContent:\t" + self.content +
                "\nDist:\t\t" + self.dist +
                "\nDistro:\t\t" + str(self.distro) +
                "\nEnabled:\t" + str(self.enabled) +
                "\nExists:\t\t" + str(self.exists) +
                "\nGPG check:\t" + str(self.gpgcheck) +
                "\nGPG key:\t" + self.gpgkey +
                "\nRepo GPG check:\t" + str(self.repo_gpgcheck) +
                "\nKeep:\t\t" + str(self.keep) +
                "\nTags:\t\t" + self.tags +
                "\nType:\t\t" + self.type +
                "\nDemote to:\t" + ", ".join(self.demote_repo_ids) +
                "\nPromote to:\t" + ", ".join(self.promote_repo_ids) +
                "\nDownstream:\t" + ", ".join(self.downstream_urls) +
                "\nUpstream:\t" + ", ".join(self.upstream_urls) +
                "\n")
