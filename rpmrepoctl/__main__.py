#!/usr/bin/env python3

"""Command line interface for the rpmrepoctl utility."""

import logging
from rpmrepoctl.errors import RpmRepoCtlError
from rpmrepoctl.rpmrepoctl import RpmRepoCtl


__author__ = "Daniel J. R. May"
__copyright__ = "Copyright 2018, Daniel J. R. May"
__credits__ = ["Daniel J. R. May"]
__license__ = "GPLv3"
__version__ = "0.10.0"
__maintainer__ = "Daniel J. R. May"
__email__ = "daniel.may@danieljrmay.com"
__status__ = "Prototype"


def main():
    """Entry point for rpmrepoctl command line application."""
    try:
        rpmrepoctl = RpmRepoCtl(args=None, debug=False)
        rpmrepoctl.execute()
        return 0
    except (RpmRepoCtlError) as rpmrepoctl_error:
        logging.error(str(rpmrepoctl_error))
        return rpmrepoctl_error.exit_status


if __name__ == '__main__':
    exit(main())
