"""All configuration related code."""

import argparse
import configparser
import glob
import logging
import os
from rpmrepoctl.errors import NoConfigFilesError
from rpmrepoctl.errors import NoConfigForRepoError
from rpmrepoctl.errors import NonExistantConfigDirError
from rpmrepoctl.errors import UnreadableConfigDirError
from rpmrepoctl.repository import Repository
from rpmrepoctl.rpm import Rpm
from rpmrepoctl.variables import Variables


class RpmRepoCtl():
    """Representation of the configuration passed in by the command line
    arguments and that loaded from the *.rpmrepoctl files."""

    @staticmethod
    def get_argument_parser():
        """Return the command line arguments parser."""
        parser = argparse.ArgumentParser(
            prog="rpmrepoctl",
            description="Utility for creating and managing RPM repositories",
            epilog="For more information see "
            "https://github.com/danieljrmay/rpmrepoctl or the man page"
        )

        parser.add_argument(
            "-d", "--debug",
            action="store_true",
            help="debugging operation, show debugging messages"
        )

        parser.add_argument(
            "-v", "--verbose",
            action="store_true",
            help="verbose operation, show lots of messages"
        )

        parser.add_argument(
            "-y", "--assumeyes",
            action="store_true",
            help="automatically answer yes to all questions"
        )

        parser.add_argument(
            "--skip-update",
            action="store_true",
            help="skip repository update"
        )

        parser.add_argument(
            "--repo",
            action="append",
            help="operate only on the specified repository, "
            "can be used multiple times"
        )

        parser.add_argument(
            "--arch",
            help="override the arch variable value"
        )

        parser.add_argument(
            "--basearch",
            help="override the basearch variable value"
        )

        parser.add_argument(
            "--releasever",
            help="override the releasever variable value"
        )

        parser.add_argument(
            "--repos-config-dir",
            default="/etc/rpmrepoctl.d",
            help="specify the directory which contains the "
            "repository configuration files"
        )

        parser.add_argument(
            "--keep",
            help="override number of old RPMs to keep when cleaning"
        )

        parser.add_argument(
            "--deltas",
            action="store_true",
            help="force the creation of deltarpms"
        )

        parser.add_argument(
            "command",
            choices=["add", "clean", "create", "demote", "destroy", "help",
                     "list", "promote", "pull", "push", "remove", "repoinfo",
                     "repolist", "rpminfo", "update"],
            help="the command to execute"
        )

        parser.add_argument(
            "rpms",
            nargs=argparse.REMAINDER,
            help="the RPMs to operate on"
        )

        return parser

    def __init__(self, args=None, debug=False):
        """Constructor."""
        if args:
            self._args = args
        else:
            parser = RpmRepoCtl.get_argument_parser()
            self._args = parser.parse_args()

        if debug or self._args.debug:
            log_level = logging.DEBUG
        elif self._args.verbose:
            log_level = logging.INFO
        else:
            log_level = logging.WARNING

        logging.basicConfig(
            format="[%(levelname)s] %(message)s",
            level=log_level)

        logging.debug("Debugging mode = %s", str(debug))
        logging.debug("Command line arguments = %s", str(self._args))

        variables = Variables()
        if self._args.arch:
            variables.override_arch(self._args.arch)

        if self._args.basearch:
            variables.override_basearch(self._args.basearch)

        if self._args.releasever:
            variables.override_releasever(self._args.releasever)

        self._config = configparser.ConfigParser(
            variables.dictionary,
            interpolation=configparser.ExtendedInterpolation())

        for path in self._repo_config_paths:
            logging.info("Reading repository configuration file: %s", path)
            self._config.read(path)

    @property
    def assume_yes(self):
        """True if we should assume "yes" to any questions."""
        return self._args.assumeyes

    @property
    def keep(self):
        """Return the number of RPMs to keep when cleaning as specified by the
        command line arguments."""
        return self._args.keep

    @property
    def repo_id_list(self):
        """Return a list repo_ids.

        If specific repo-ids are specified in _args (probably via the
        command line --repo switch. We check that they are valid,
        raising a ValueError if a requested repo-id does not exist in
        the configuration files.

        If no specific repo-ids were requested in _args, we get a list
        of all repo-ids which exist in the configuration files.
        """
        if not self._args.repo:
            return self._config.sections()

        repo_id_list = []
        for repo_id in self._args.repo:
            if self.has_repo_id(repo_id):
                repo_id_list.append(repo_id)
            else:
                raise NoConfigForRepoError(repo_id)

        return repo_id_list

    @property
    def rpm_list(self):
        """Get a list of Rpm objects corresponding to the list of RPM paths
        specified on the command line.
        """
        rpm_path_list = self.rpm_path_list
        rpm_list = []
        if rpm_path_list:
            for path in rpm_path_list:
                rpm = Rpm(path)
                logging.debug(
                    "Metadata for %s follows:\n%s",
                    rpm.filename, rpm)
                rpm_list.append(rpm)

        return rpm_list

    @property
    def rpm_path_list(self):
        """Return a list of RPM file paths as specified on the command line."""
        return self._args.rpms

    @property
    def skip_update(self):
        """True if we should skip repository updates."""
        return self._args.skip_update

    @property
    def verbose(self):
        """True if verbose messaging should be used."""
        return self._args.verbose

    @property
    def _repo_config_paths(self):
        """Return list of readable *.rpmrepoctl configuration file paths."""
        repos_config_dir = self._args.repos_config_dir

        if not os.path.isdir(repos_config_dir):
            raise NonExistantConfigDirError(repos_config_dir)

        if not os.access(repos_config_dir, os.R_OK):
            raise UnreadableConfigDirError(repos_config_dir)

        paths = glob.glob(repos_config_dir + "/*.rpmrepoctl")
        readable_paths = []
        for path in paths:
            if os.access(path, os.R_OK):
                readable_paths.append(path)
            else:
                logging.warning(
                    "The repository configuration file %s "
                    "is not readable.", path)

        if not readable_paths:
            raise NoConfigFilesError(repos_config_dir)

        logging.debug(
            "Found the following *.rpmrepoctl paths: %s",
            str(readable_paths))
        return readable_paths

    def add(self):
        """Add RPMs to repositories."""
        for repo_id in self.repo_id_list:
            repo = self.get_repo(repo_id)

            if repo.exists:
                repo.add(self.rpm_list, self.skip_update)
            else:
                question = (
                    "Repository {} at {} does not exist. "
                    "Do you want to create it (y/N)?"
                ).format(repo_id, repo.path)

                create_repo_and_add_rpms = self._user_confirms(
                    question,
                    ["y", "Y", "yes", "YES"])

                if create_repo_and_add_rpms:
                    repo.add(self.rpm_list, self.skip_update)
                else:
                    logging.info(
                        "Skipping add to non-existant repository %s "
                        "because user declined to create it.",
                        repo_id)

    def clean(self):
        """Clean old RPMs from repositories."""
        for repo_id in self.repo_id_list:
            repo = self.get_repo(repo_id)
            repo.clean(self.skip_update, self.keep)

    def create(self):
        """Create the repositories."""
        for repo_id in self.repo_id_list:
            repo = self.get_repo(repo_id)
            repo.create()

    def demote(self):
        """Demote RPMs from repositories."""
        for repo_id in self.repo_id_list:
            repo = self.get_repo(repo_id)
            repo.demote(self.rpm_list, self.skip_update)

    def destroy(self):
        """Destroy the repositories."""
        for repo_id in self.repo_id_list:
            repo = self.get_repo(repo_id)

            if self._user_confirms(
                    "Are you sure you want to "
                    "destroy {} (y/N)? ".format(repo_id),
                    ['y', 'Y', 'yes', 'YES']):
                repo.destroy()

    def execute(self):
        """Execute the command."""
        if self._args.command == "add":
            self.add()
        elif self._args.command == "clean":
            self.clean()
        elif self._args.command == "create":
            self.create()
        elif self._args.command == "demote":
            self.demote()
        elif self._args.command == "destroy":
            self.destroy()
        elif self._args.command == "help":
            self.print_help()
        elif self._args.command == "list":
            self.rpmlist()
        elif self._args.command == "promote":
            self.promote()
        elif self._args.command == "pull":
            self.pull()
        elif self._args.command == "push":
            self.push()
        elif self._args.command == "remove":
            self.remove()
        elif self._args.command == "repoinfo":
            self.repoinfo()
        elif self._args.command == "repolist":
            self.repolist()
        elif self._args.command == "rpminfo":
            self.rpminfo()
        elif self._args.command == "update":
            self.update()
        else:
            raise AssertionError(
                "Illegal value for command='{}'.".format(self._args.command))

    def get_repo(self, repo_id):
        """Return the specified repository."""
        if not self.has_repo_id(repo_id):
            raise NoConfigForRepoError(repo_id)

        return Repository(repo_id, self._config)

    def has_repo_id(self, repo_id):
        """Return True if the specifed repo_id exists in the current
        configuration, False otherwise."""
        return self._config.has_section(repo_id)

    def print_help(self):
        """Print help information."""
        print("This is the help text.")
        print("Repository IDs: " + ", ".join(self.repo_id_list))

    def promote(self):
        """Promote specified RPMs from specifed repositories."""
        for repo_id in self.repo_id_list:
            repo = self.get_repo(repo_id)
            repo.promote(self.rpm_list, self.skip_update)

    def pull(self):
        """Pull RPMs from upstream to specifed repositories."""
        for repo_id in self.repo_id_list:
            repo = self.get_repo(repo_id)
            repo.pull(self.skip_update)

    def push(self):
        """Push RPMs from specifed repositories downstream."""
        for repo_id in self.repo_id_list:
            repo = self.get_repo(repo_id)
            repo.push(self.skip_update)

    def remove(self):
        """Remove specified RPMs from specifed repositories."""
        for repo_id in self.repo_id_list:
            repo = self.get_repo(repo_id)
            repo.remove(self.rpm_list, self.skip_update)

    def repoinfo(self):
        """Print infomation about the specified repositorys."""
        for repo_id in self.repo_id_list:
            repo = self.get_repo(repo_id)
            print(repo)

    def repolist(self):
        """Print list of available repository IDs."""
        for repo_id in self.repo_id_list:
            print(repo_id)

    def rpminfo(self):
        """Print information about the specified RPM files."""
        for rpm in self.rpm_list:
            print(rpm)

    def rpmlist(self):
        """Print RPMs in specified repositories."""
        for repo_id in self.repo_id_list:
            repo = self.get_repo(repo_id)
            rpm_filenames = repo.list()
            if rpm_filenames:
                print("\n".join(rpm_filenames))

    def update(self):
        """Update specifed repositories."""
        for repo_id in self.repo_id_list:
            repo = self.get_repo(repo_id)
            repo.update()

    def update_variables(self, variables):
        """Update the variables used by this configuration."""
        for key, value in variables.dictionary.items():
            self._config.set("DEFAULT", key, value)

    def _user_confirms(self, question, truthy_responses):
        """Get confirmation from the user."""
        if self.assume_yes:
            return True

        answer = input(question)

        if answer in truthy_responses:
            return True

        return False
