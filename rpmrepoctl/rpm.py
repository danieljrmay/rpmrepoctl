"""Rpm file related code."""

import os
import re
import subprocess


class Rpm():
    """Represents a RPM file.

    There is some good documentation about RPM head tags at
    https://docs.fedoraproject.org/en-US/Fedora_Draft_Documentation/0.1/html/RPM_Guide/ch-package-structure.html
    """

    QUERYFORMAT_REGEX = re.compile(
        r"^NAME=(.*)$\n"
        r"^VERSION=(.*)$\n"
        r"^RELEASE=(.*)$\n"
        r"^DISTRIBUTION=(.*)$\n"
        r"^ARCH=(.*)$\n"
        r"^SOURCEPACKAGE=(.*)$\n"
        r"^SIGNATURE=(.*)$\n",
        re.MULTILINE
    )

    RELEASE_REGEX = re.compile(r"^(\d+)\.*(el|fc)(\d+)(.*)$")

    SIGNATURE_REGEX = re.compile(r"^(.+),\ (.+),\ Key\ ID\ (.+)$")

    def __init__(self, path):
        """Constructor."""
        self.path = path

        cmd = ["rpm", "--query", "--package",
               "--queryformat",
               "NAME=%{NAME}\n" +
               "VERSION=%{VERSION}\n" +
               "RELEASE=%{RELEASE}\n" +
               "DISTRIBUTION=%{DISTRIBUTION}\n" +
               "ARCH=%{ARCH}\n" +
               "SOURCEPACKAGE=%{SOURCEPACKAGE}\n" +
               "SIGNATURE=%{SIGPGP:pgpsig}\n",
               path]
        completed_process = subprocess.run(cmd, capture_output=True, text=True)
        completed_process.check_returncode()
        match = Rpm.QUERYFORMAT_REGEX.fullmatch(completed_process.stdout)
        if match:
            self.name = match.group(1)
            self.version = match.group(2)
            self.release = match.group(3)
            self.distribution = match.group(4)
            self.arch = match.group(5)
            self.is_source = bool(match.group(6) == "1")
            self.signature = match.group(7)

        release_match = Rpm.RELEASE_REGEX.fullmatch(self.release)
        if release_match:
            self.specver = release_match.group(1)
            self.distro_code = release_match.group(2)
            self.releasever = release_match.group(3)

        self.signature_format = None
        self.signature_date = None
        self.signature_keyid = None

        if self.signature != "(none)":
            signature_match = Rpm.SIGNATURE_REGEX.fullmatch(self.signature)
            if signature_match:
                self.signature_format = signature_match.group(1)
                self.signature_date = signature_match.group(2)
                self.signature_keyid = signature_match.group(3)

    @property
    def dist(self):
        """Get the dist for this RPM e.g. ".fc30", ".el7", etc."""
        return "." + self.distro_code + self.releasever

    @property
    def filename(self):
        """Get the RPMs filename."""
        return os.path.basename(self.path)

    def info(self):
        """Get information string about an RPM."""
        cmd = ["rpm", "--query", "--package", "--info", self.path]
        completed_process = subprocess.run(cmd, capture_output=True, text=True)
        completed_process.check_returncode()
        return completed_process.stdout

    def __str__(self):
        """Return an informal string representation of this Rpm.

        This magic method controls the output of str(rpm) and
        print(rpm).
        """
        return "Path        : " + self.path + "\n" + self.info()

    def debug(self):
        """Print the properties of this RPM."""
        print("path=" + self.path)
        print("filename=" + self.filename)
        print("name=" + self.name)
        print("version=" + self.version)
        print("release=" + self.release)
        print("distribution=" + self.distribution)
        print("specver=" + self.specver)
        print("distro_code=" + self.distro_code)
        print("releasever=" + self.releasever)
        print("arch=" + self.arch)
        print("is_source=" + str(self.is_source))
        print("signature=" + self.signature)
        print("signature_format=" + str(self.signature_format))
        print("signature_date=" + str(self.signature_date))
        print("signature_keyid=" + str(self.signature_keyid))
