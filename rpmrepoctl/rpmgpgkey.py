"""RPM GPG key related code."""

import os

import gnupg


class RpmGpgKey():
    """Represents a RPM GPG key file."""

    DEFAULT_RPM_GPG_KEY_DIRECTORY = "/etc/pki/rpm-gpg"

    @staticmethod
    def get_keys(path):
        """Get list of keyids represented by key files."""
        gpg = gnupg.GPG(gnupghome='/tmp')
        return gpg.scan_keys(path)

    @staticmethod
    def file_contains_key(path, keyid):
        """True if path contains key with specified keyid."""
        for key in RpmGpgKey.get_keys(path):
            if key["keyid"].upper().endswith(keyid.upper()):
                return True

        return False

    @staticmethod
    def get_key_file_paths(directory=DEFAULT_RPM_GPG_KEY_DIRECTORY):
        """Get a list of potential key file paths from local filesystem."""
        paths = []
        for filename in os.listdir(directory):
            path = os.path.join(directory, filename)
            if (
                    os.path.isfile(path) and
                    not os.path.islink(path)):
                paths.append(path)

        return paths

    @staticmethod
    def find_key_file_path(keyid, directory=DEFAULT_RPM_GPG_KEY_DIRECTORY):
        """Find the path for for the specified keyid."""
        for path in RpmGpgKey.get_key_file_paths(directory):
            if RpmGpgKey.file_contains_key(path, keyid):
                return path

        return False
