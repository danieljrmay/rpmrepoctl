"""Errors and exit codes from rpmrepoctl."""


class RpmRepoCtlError(Exception):
    """Base class for all rpmrepoctl exceptions."""

    EXIT_OK = 0
    EXIT_VALUE_ERROR = 1
    EXIT_NON_EXISTANT_CONFIG_DIR = 2
    EXIT_PERMISSION_ERROR = 3
    EXIT_FILE_NOT_FOUND_ERROR = 4
    EXIT_CONFIGURATION_ERROR = 5
    EXIT_UNREADABLE_CONFIG_DIR = 6
    EXIT_NO_CONFIG_FILES = 7

    def __init__(self, message, exit_status):
        super().__init__(message)
        self.exit_status = exit_status


class NoConfigForRepoError(RpmRepoCtlError):
    """No configuration exists for a repository ID."""

    def __init__(self, repo_id):
        super().__init__(
            "No configuration exists for the "
            "repository ID '{}'.".format(repo_id),
            self.EXIT_CONFIGURATION_ERROR)


class NonExistantConfigDirError(RpmRepoCtlError):
    """The specified configuration directory does not exist."""

    def __init__(self, repos_config_dir):
        super().__init__(
            "The specified repos_config_dir={} "
            "is not a directory.".format(repos_config_dir),
            self.EXIT_NON_EXISTANT_CONFIG_DIR)


class UnreadableConfigDirError(RpmRepoCtlError):
    """The specified configuration directory does not exist."""

    def __init__(self, repos_config_dir):
        super().__init__(
            "The specified repos_config_dir={} "
            "is not readable.".format(repos_config_dir),
            self.EXIT_UNREADABLE_CONFIG_DIR)


class NoConfigFilesError(RpmRepoCtlError):
    """No configuration files."""

    def __init__(self, repos_config_dir):
        super().__init__(
            "No readable *.rpmrepoctl repository configuration "
            "files were found in the {} directory.".format(repos_config_dir),
            self.EXIT_NO_CONFIG_FILES)
